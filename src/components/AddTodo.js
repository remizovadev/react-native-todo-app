import React, {useState} from 'react'
import {View, StyleSheet, TextInput, Button, Alert} from 'react-native'
import {THEME} from "../theme";

export const AddTodo = ({onSubmit}) => {

    const [value, setValue] = useState('');

    const pressHandler = () => {
        if (value.trim()) {
            onSubmit(value);
            setValue('')
        } else {
            Alert.alert('Название дела не может быть пустым!')
        }
    };
    return (
        <View style={styles.block}>
            <TextInput
                onChangeText={setValue}
                value={value}
                style={styles.input}
                placeholder="Введите название дела..."
                autoCorrect={false}
                autoCapitalize='none'
            />
            <Button style={styles.button} title="Добавить" onPress={pressHandler}/>
        </View>
    )
};

const styles = StyleSheet.create({
    block: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 15
    },
    input: {
        width: '60%',
        padding: 10,
        borderStyle: 'solid',
        borderBottomColor: THEME.MAIN_COLOR,
        borderBottomWidth: 2,
        marginRight: 10
    },
    button: {
        height: 50,
        alignItems: 'center'
    }
});
