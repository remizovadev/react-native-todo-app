import React from 'react'
import {StyleSheet, TouchableOpacity, View} from 'react-native'
import {AppText} from "./ui/AppText";

export const Todo = ({todo, onRemove, onOpen}) => {
    const longPressHandler = () => {
        onRemove(todo.id)
    };

    return (
        <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => onOpen(todo.id)}
            onLongPress={longPressHandler}
            // onLongPress={onRemove.bind(null, todo.id)}
            // onLongPress={() => onRemove(todo.id)}
        >
            <View style={styles.todo}>
                <AppText>{todo.title}</AppText>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    todo: {
        flexDirection: 'row',
        alignSelf: 'center',
        padding: 15,
        borderWidth: 1,
        borderColor: '#eee',
        borderRadius: 5,
        marginBottom: 10,
        width: '100%'
    }
});
